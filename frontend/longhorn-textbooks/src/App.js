import React from 'react';
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Link
} from "react-router-dom";
import './App.css';
import Home from './components/Home.jsx';
import Course from './components/Course.jsx';
import Book from './components/Book.jsx'


function App() {
  return (<Router>
    <div>
      <nav>
        <ul>
          <li>
            <Link to="/">Home</Link>
          </li>
          <li>
            <Link to="/course/123">Course</Link>
          </li>
          <li>
            <Link to="/book/123">Book</Link>
          </li>
        </ul>
      </nav>

      {/* A <Switch> looks through its children <Route>s and
          renders the first one that matches the current URL. */}
      <Switch>
        <Route path="/course/:unique" component={Course} />
        <Route path="/book/:isbn" component={Book} />
        <Route path="/">
          <Home />
        </Route>
      </Switch>
    </div>
  </Router>);
}

export default App;