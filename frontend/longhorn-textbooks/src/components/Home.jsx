import React from 'react';
import '../css/Home.css'

class Home extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            search_value: "",
        };


        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    getRandomCourse() {
        fetch('http://localhost:5000/random/course')
            .then(response => {
                if (response.ok) {
                    return response.json();
                } else {
                    throw new Error('Something went wrong');
                }
            })
            .then(data => window.location.href = "/course/" + data)
            .catch(error => this.setState({ error }));
    }

    handleChange(event) {
        this.setState({ search_value: event.target.value });

    }

    handleSubmit(event) {
        let uniqueNum = this.state.search_value;
        // TODO : validate here

        window.location.href = '/course/' + uniqueNum;

        event.preventDefault();

    }

    render() {
        return (
            <div className="hero-image">
                <div className="hero-text">
                    <h1>Longhorn Textbooks</h1>
                    <p>Download your textbooks for all your courses here for free</p>
                    <button onClick={() => this.getRandomCourse()} >Random Course</button>
                    <form onSubmit={this.handleSubmit}>
                        {/* Shorten the search bar to imply that only short numbers are needed */}
                        <input type="text" id="search" placeholder="55555" value={this.state.search_value} onChange={this.handleChange.bind(this)} />
                        <button type="submit">Search</button>
                    </form>

                </div>
            </div>
        );
    }
}

export default Home;