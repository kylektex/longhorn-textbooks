import React from 'react';
import {
    BrowserRouter,
    Link,
} from "react-router-dom";
import BookCard from './BookCard.jsx';
import '../css/Course.css';



class Course extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            data: {
                books: [],
            },
            isLoading: false,
            error: null,
        };
    }

    componentDidMount() {
        const unique = this.props.match.params.unique;
        this.setState({ isLoading: true });
        fetch('http://localhost:5000/course/' + unique)
            .then(response => {
                if (response.ok) {
                    return response.json();
                } else {
                    throw new Error('Something went wrong');
                }
            })
            .then(data => this.setState({ data: data, isLoading: false }))
            .catch(error => this.setState({ error, isLoading: false }));

    }

    render() {
        const { data, isLoading } = this.state;
        if (isLoading) {
            return (
                <p>loading...</p>
            )
        }

        const url = "http://93.174.95.29";
        return (
            <div className="container">
                {this.state.data.unique}
                <h1>{this.state.data.title}</h1>
                <h3>by {this.state.data.prof}</h3>
                <p>{this.state.data.description}</p>
                {/* TODO: put this below into a component*/}
                <p>Textbooks ({this.state.data.books.length}):</p>
                <ul className="books-list">
                    {this.state.data.books.length == 0 &&
                        <p>The professor listed no books for this course.</p>
                    }
                    {
                        this.state.data.books.map(item => (
                            <BookCard book={item} />
                        ))
                    }
                </ul>

            </div>
        )
    }
}

export default Course;