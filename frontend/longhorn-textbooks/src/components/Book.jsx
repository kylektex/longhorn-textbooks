import React from 'react';
import '../css/Book.css';

class Book extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            data: {},
            isLoading: false,
            error: null,
        };
    }

    componentDidMount() {
        const isbn = this.props.match.params.isbn;
        this.setState({ isLoading: true });
        fetch('http://localhost:5000/book/' + isbn)
            .then(response => {
                if (response.ok) {
                    return response.json();
                } else {
                    throw new Error('Something went wrong');
                }
            })
            .then(data => this.setState({ data: data, isLoading: false }))
            .catch(error => this.setState({ error, isLoading: false }));
    }

    render() {
        const { data, isLoading } = this.state;
        if (isLoading) {
            return (
                <p>loading...</p>
            )
        }
        if (!data.download_link) { //change to has_download
            return (
                <p>no book found for this course</p>
            )
        }
        const url = "http://93.174.95.29";
        console.log(url + data.download_link)
        return (
            <div className="container">
                <div className="book">
                    <div className="book-cover">
                        <img src={url + data.image_url} alt="Textbook cover" />
                    </div>
                    <div className="book-aside">
                        <h1>{data.title}</h1>
                        <h2>by {data.author}</h2>
                        <h3>{data.required}</h3>
                        <p>{data.description}</p>
                        <a href={url + data.download_link} class="btn btn-primary" target="_blank" rel="noopener noreferrer">Download</a>
                    </div>
                </div>



            </div>
        )
    }
}

export default Book;