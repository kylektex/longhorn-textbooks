import React from 'react';
import {
    BrowserRouter,
    Link,
} from "react-router-dom";
import '../css/BookCard.css'


class BookCard extends React.Component {
    constructor(props) {
        super(props);
    }


    render() {
        const url = "http://93.174.95.29";
        let b = this.props.book
        if (b.download_link) {
            return (
                <Link to={"/book/" + b.isbn}>
                    <li className="book-card" key={b.isbn}>
                        <h3>{b.title}</h3>
                        <h4>by {b.author}</h4>
                        {b.required}
                        <img class="book-card-img" src={url + b.image_url} />
                    </li>
                </Link>
            )
        }

        return (
            <Link to={"/book/" + b.isbn}>
                <li className="book-card" key={b.isbn}>
                    <h3>{b.title}</h3>
                    <h4>by {b.author}</h4>
                    {b.required}
                    <div class="book-card-img not-available">
                        <div class="book-not-available">Book Not Available</div>
                    </div>

                </li>
            </Link >
        )
    }
}

export default BookCard;