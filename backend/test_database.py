from models import Course, Book
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker

engine = create_engine('sqlite:///textbooks-2020.db')
Session = sessionmaker(bind=engine)

session = Session()

print('Courses')
having_books = []
for c in session.query(Course).all():
    having_books.append((len(c.books), c.unique))
    print(c.__dict__)

print()
print('Books')

for b in session.query(Book).all():
    print(b.__dict__)

print(having_books)


our_course = session.query(Course).count()
print("found " + str(our_course) + " courses")

num_books = session.query(Book).count()
print("found " + str(num_books) + " books")
