from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy import Column, Integer, String, Boolean, Text, ForeignKey, Table
from sqlalchemy.orm import relationship

Base = declarative_base()

association_table = Table('association', Base.metadata,
                          Column('course_id', Integer,
                                 ForeignKey('course.unique')),
                          Column('book_id', Integer, ForeignKey('book.isbn'))
                          )

# professor photo?


class Course(Base):
    __tablename__ = 'course'
    unique = Column(Integer, primary_key=True)
    semester = Column(Integer)
    prof = Column(String)
    title = Column(String)
    link = Column(String)
    description = Column(Text)
    has_books = Column(Boolean)  # make sure this is filled out
    books = relationship('Book', secondary=association_table)

    def as_dict(self):
        return {c.name: getattr(self, c.name) for c in self.__table__.columns}

    def __repr__(self):
        return '<Class %s>' % self.title


class Book(Base):
    __tablename__ = 'book'
    # id = Column(Integer, primary_key=True)
    isbn = Column(String, primary_key=True)
    title = Column(String)
    author = Column(String)
    required = Column(String)
    image_url = Column(String)
    description = Column(Text)
    download_link = Column(String)
    # course_unique = Column(Integer, ForeignKey('course.unique'))

    def as_dict(self):
        return {c.name: getattr(self, c.name) for c in self.__table__.columns}

    def __repr__(self):
        return '<Book %s>' % self.title
