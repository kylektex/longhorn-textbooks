from flask import Flask, send_from_directory, url_for, redirect
from flask_cors import CORS
from libgen import get_book
import json
import random
from models import Course, Book
from database import session

app = Flask(__name__)
CORS(app)

# set up search from the home page


@app.route('/random/course')
def random_course():
    query = session.query(Course)
    num_rows = int(query.count())
    random_row = query.offset(int(num_rows * random.random())).first()
    course_num = int(random_row.unique)
    return str(course_num)


@app.route('/course/<int:num>')
def send_course(num):
    course = session.query(Course).get(num)  # todo: get or 404
    my_courses = course.as_dict()
    books = []
    for b in course.books:
        books.append(b.as_dict())
    my_courses['books'] = books
    return json.dumps(my_courses)


@app.route('/book/<int:num>')
def send_book(num):
    book = session.query(Book).get(num)
    my_book = book.as_dict()
    return json.dumps(my_book)


if __name__ == '__main__':
    app.debug = True
    app.run(host="0.0.0.0")
