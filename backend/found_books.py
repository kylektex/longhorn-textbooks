from models import Course, Book
from database import session

print('Books')

count = 0

for b in session.query(Book).filter(Book.download_link == None).all():
    count += 1
    print(b.__dict__)

total_books = session.query(Book).count()

print("Found " + str(total_books - count) +
      " downloadable books out of " + str(total_books) + " total books")
