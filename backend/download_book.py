import requests

url = 'http://93.174.95.29/main/2361000/3c1849af3289bab50a2b11d07c097c41/%28Volume%200%20of%20Norton%20Global%20Ethics%20Series%29%20John%20Broome%20-%20Climate%20Matters_%20Ethics%20in%20a%20Warming%20World%20%28Norton%20Global%20Ethics%20Series%29-W.%20W.%20Norton%20%282012%29.epub'
r = requests.get(url, stream=True)

chunk_size = 2000
with open('./climate_matters.pdf', 'wb') as fd:
    for chunk in r.iter_content(chunk_size):
        fd.write(chunk)
