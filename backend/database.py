from models import Course, Book
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker

# New engine

engine = create_engine('sqlite:///textbooks-2020.db')
Session = sessionmaker(bind=engine)

session = Session()

# Old engine

old_engine = create_engine('sqlite:///textbooks.db')
old_Session = sessionmaker(bind=old_engine)

old_session = old_Session()
