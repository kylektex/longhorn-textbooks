#!/usr/bin/env python3

from selenium import webdriver
from time import sleep

driver = webdriver.Firefox()
driver.get('https://utdirect.utexas.edu/apps/registrar/course_schedule/20202/results/?ccyys=20202&search_type_main=UNIQUE&unique_number=&start_unique=00000&end_unique=99999&x=114&y=20')

username = driver.find_element_by_id("IDToken1")
password = driver.find_element_by_id("IDToken2")

username.send_keys("ktk536")
password.send_keys("Klkknght9753!")

driver.find_element_by_css_selector('input.login').click()
driver.implicitly_wait(3)

while True:
    unique_nums_element = driver.find_elements_by_css_selector(
        'a[title="Unique number"]')

    with open("unique_nums_sp2020.txt", "a+") as f:
        for i in unique_nums_element:
            f.write(i.text + "\n")

    next_button = driver.find_element_by_id("next_nav_link")

    if not next_button:
        print("no next button")
        break

    sleep(0.3)
    next_button.click()
