import requests
from bs4 import BeautifulSoup
from database import session
from database import old_session
from models import Course, Book
from time import sleep


# Step 1
"""
Better algorithm for finding books:

1. try book name + author name
2. try book name - 1 word + author name (if book name has at least 2 words)
3. try book name - 2 word + author name
4. try book name - 3 word + author name

*all other books can't be found
"""
# Step 2
"""
Download the rest of the courses
Run improved book fetching
Fix jank UI
"""

# TODO: write script for manually adding/updating book url
# check for no book required for this course


def get_book(isbn):
    # look up isbn in my database
    s = ""

    for u in session.query(Book).all():
        s += str(u.__dict__)

    return s


def run_search(query):
    print("query is " + query)
    r = requests.get(
        'http://gen.lib.rus.ec/search.php?&req=' + query + '&phrase=0&view=simple&column=def&sort=year&sortmode=DESC')

    # print(r.content)

    soup = BeautifulSoup(r.content, 'html.parser')

    link_element = soup.select_one(
        'table.c > tr:nth-of-type(2) > td:nth-of-type(10) > a')

    try:
        link = link_element['href']
        print("found link href")
        return link
    except TypeError:
        # returned no results
        print('link href not found')
        return None


def search_for_book(book, title, author):
    query = book.title
    if author:
        query += " " + author
    link = run_search(query)

    words_to_trim = 1
    while link is None and words_to_trim <= 3:
        if len(book.title.split()) < words_to_trim + 1:
            break
        query = " ".join(book.title.split()[:-words_to_trim])
        if author:
            query += " " + author
        link = run_search(query)
        words_to_trim += 1

    # now try just the title, no author

    return link


def update_book(book, title, author):
    # check if book has been found already
    db_book = session.query(Book).get(book.isbn)
    if db_book.download_link:
        # already found
        return
    print(book.isbn)
    sleep(1)
    link = search_for_book(book, title, author)
    if link is None:
        # couldn't find the book
        return

    print("link: " + link)
    sleep(1)

    r = requests.get(link)

    # print(r.content)

    soup = BeautifulSoup(r.content, 'html.parser')

    desc_element = soup.select_one('#info > div:nth-of-type(2)')
    if desc_element is not None:
        # print(desc_element.text)

        book.description = desc_element.text

    img_element = soup.select_one('img')
    if img_element is not None:
        img_src = img_element['src']
        # print('http://93.174.95.29' + img_src)

        book.image_url = img_src

    download_element = soup.select_one('h2 > a')

    if download_element is not None:
        download_link = download_element['href']

        # filename = download_link.split('/')[-1]

        print('http://93.174.95.29' + download_link)

        book.download_link = download_link

    session.commit()


if __name__ == "__main__":
    # for each book in database
    for b in session.query(Book).all():
        if not b.download_link:
            # check if isbn is in old database
            old_book = old_session.query(Book).get(b.isbn)
            if old_book and old_book.download_link:
                print("cache hit")
                print(old_book)
                b.download_link = old_book.download_link
                b.description = old_book.description
                b.image_url = old_book.image_url
                continue
            print("cache miss")

            title = b.title
            try:
                author = b.author.split()[0]
            except:
                author = None
            print('Trying book ' + title)
            update_book(b, title, author)

    for b in session.query(Book).all():
        print(b.__dict__)

    session.close()
