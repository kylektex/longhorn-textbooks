from sqlalchemy import create_engine
from sqlalchemy.orm import relationship, sessionmaker
from sqlalchemy.ext.declarative import declarative_base
from selenium import webdriver
from time import sleep, time
import pickle
from models import Course, Book
from selenium.common.exceptions import NoSuchElementException

start_time = time()

semester_code = 20202

engine = create_engine('sqlite:///textbooks-2020.db')
Session = sessionmaker(bind=engine)

# TODO: get course nums that start with zeros

# need to comment this out after running
# Course.metadata.drop_all(engine)
# Course.metadata.create_all(engine)
# Book.metadata.drop_all(engine)
# Book.metadata.create_all(engine)

session = Session()


# last = 0
# with open('last_saved.pickle', 'rb') as f:
#     last = pickle.load(f)

# last = 0


# use pickle to keep track of the current state
driver = webdriver.Firefox(executable_path='geckodriver')
driver.get(
    'https://utdirect.utexas.edu/apps/registrar/course_schedule/{}/'.format(semester_code))

username = driver.find_element_by_id("IDToken1")
password = driver.find_element_by_id("IDToken2")

username.send_keys("ktk536")
password.send_keys("Klkknght9753!")

driver.find_element_by_css_selector('input.login').click()


last = 60729

with open('unique_nums_sp2020.txt', 'r') as f:
    for line in f:
        num = int(line)

        if num <= last:
            continue

        if session.query(Course).get(num) is not None:
            print("skipping")
            continue

        # print(session.query(Course).get(num))

        num_str = f'{num:05}'
        print(num_str)

        driver.get(
            'https://utdirect.utexas.edu/apps/registrar/course_schedule/{0}/{1}/'.format(semester_code, num_str))
        driver.implicitly_wait(3)
        link = driver.find_element_by_id('modal_button').get_attribute('href')
        try:
            title = driver.find_element_by_css_selector('#details>h2').text
        except NoSuchElementException:
            # no course found, so skip
            continue
        prof = driver.find_element_by_css_selector(
            'td[data-th="Instructor"]').text
        # join each paragraph in the description together
        desc = "\n".join(
            [e.text for e in driver.find_elements_by_css_selector('#details>p')])
        print(desc)
        print(num)
        print(title)
        print(link)
        print(prof)

        driver.get(link)
        all_books = []
        # first book
        try:
            container = driver.find_element_by_css_selector(
                '.adoptionsearch-list-course-materials-item-details-selected')
        except NoSuchElementException:
            print('The professor didn\'t list a book for this course')
            course = Course(unique=num, prof=prof, title=title,
                            link=link, description=desc, has_books=False, books=[])
            session.add(course)
            session.commit()
            # update pickle here
            # with open('last_saved.pickle', 'wb') as f:
            #     pickle.dump(num, f, pickle.HIGHEST_PROTOCOL)
            continue
        b = {}
        try:
            book_title = container.find_element_by_tag_name('b').text
        except NoSuchElementException:
            print('The professor didn\'t list a book for this course')
            course = Course(unique=num, prof=prof, title=title,
                            link=link, description=desc, has_books=False, books=[])
            session.add(course)
            session.commit()
            # update pickle here
            # with open('last_saved.pickle', 'wb') as f:
            #     pickle.dump(num, f, pickle.HIGHEST_PROTOCOL)
            continue
        print(book_title)
        b['title'] = book_title
        try:
            book_author = container.find_element_by_xpath(
                './/p[starts-with(text(), \'by \')]').text[3:]  # get rid of "by Author"
            print(book_author)
            b['author'] = book_author
        except NoSuchElementException:
            # rip
            print('book doesnt have an author')
        try:
            book_isbn = container.find_element_by_xpath(
                './/p[starts-with(text(), \'ISBN: \')]').text[6:]
        except NoSuchElementException:
            continue
        print(book_isbn)
        b['isbn'] = book_isbn
        try:
            book_required = container.find_element_by_css_selector(
                'p.adoptionsearch-list-course-materials-required').text
            print(book_required)
            b['required'] = book_required
        except NoSuchElementException:
            # rip
            print('counldnt find if book was required or not')

        all_books.append(b)

        # other books
        for container in driver.find_elements_by_css_selector('.adoptionsearch-list-course-materials-item-details'):
            b = {}
            try:
                book_title = container.find_element_by_tag_name('b').text
            except NoSuchElementException:
                all_book_obj = []

                for b in all_books:
                    book = session.query(Book).get(b['isbn'])
                    if not book:
                        book = Book(**b)
                    all_book_obj.append(book)
                    session.add(book)

                print(all_book_obj)

                session.add(Course(unique=num, prof=prof, title=title,
                                   link=link, description=desc, has_books=True, books=all_book_obj))
                session.commit()
                # update pickle here
                # with open('last_saved.pickle', 'wb') as f:
                #     pickle.dump(num, f, pickle.HIGHEST_PROTOCOL)

                continue
            print(book_title)
            b['title'] = book_title
            try:
                book_author = container.find_element_by_xpath(
                    './/p[starts-with(text(), \'by \')]').text[3:]  # get rid of "by Author"
                print(book_author)
                b['author'] = book_author
            except NoSuchElementException:
                # rip
                print('book doesnt have an author')
            try:
                book_isbn = container.find_element_by_xpath(
                    './/p[starts-with(text(), \'ISBN: \')]').text[6:]
            except NoSuchElementException:
                all_book_obj = []

                for b in all_books:
                    book = session.query(Book).get(b['isbn'])
                    if not book:
                        book = Book(**b)
                    all_book_obj.append(book)
                    session.add(book)

                print(all_book_obj)

                session.add(Course(unique=num, prof=prof, title=title,
                                   link=link, description=desc, has_books=True, books=all_book_obj))
                session.commit()
                # update pickle here
                # with open('last_saved.pickle', 'wb') as f:
                #     pickle.dump(num, f, pickle.HIGHEST_PROTOCOL)
                continue
            print(book_isbn)
            b['isbn'] = book_isbn
            try:
                book_required = container.find_element_by_css_selector(
                    'p.adoptionsearch-list-course-materials-required').text
                print(book_required)
                b['required'] = book_required
            except NoSuchElementException:
                # rip
                print('counldnt find if book was required or not')
            # b['course_unique'] = num
            # b['id'] = abs(hash(frozenset(b.values())))

            all_books.append(b)

        all_book_obj = []

        for b in all_books:
            book = session.query(Book).get(b['isbn'])
            if not book:
                book = Book(**b)
            all_book_obj.append(book)
            session.add(book)

        print(all_book_obj)

        # hack i need to refactor
        if session.query(Course).get(num) is not None:
            continue

        session.add(Course(unique=num, prof=prof, title=title,
                           link=link, description=desc, has_books=True, books=all_book_obj))
        session.commit()
        # update pickle here
        # with open('last_saved.pickle', 'wb') as f:
        #     pickle.dump(num, f, pickle.HIGHEST_PROTOCOL)


end_time = time()

our_course = session.query(Course).count()
print("found " + str(our_course) + " courses")

num_books = session.query(Book).count()
print("found " + str(num_books) + " books")

print("took " + str(end_time-start_time) + " seconds to execute.")

session.close()
print('done')
